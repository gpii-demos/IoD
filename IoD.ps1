$VerbosePreference = "continue"
$GPII_Data_Path = Join-Path $env:LOCALAPPDATA "GPII"
Write-Verbose("Starting $PSCommandPath and set GPII_Data_Path: $GPII_Data_Path")

$curPath = $(get-location).Path;
$tmpDir = "$env:SystemDrive\vagrant"

choco install --yes git
refreshenv
if( Test-Path -Path $tmpDir ) {
    Remove-Item $tmpDir -Recurse -Force -Confirm:$false
}

# Retrieve current data folder
&"$env:programfiles\Git\bin\git.exe" clone "https://github.com/JavierJF/windows" $tmpDir
cd $tmpDir
&"$env:programfiles\Git\bin\git.exe" checkout GPII-2521

$chocoScript = Join-Path $tmpDir "\provisioning\Chocolatey.ps1"
$createDataDir = Join-Path $tmpDir "\provisioning\envChanges\CreateDataDir.ps1"

Invoke-Expression $chocoScript 
Invoke-Expression $createDataDir

cd $curPath
Remove-Item $tmpDir -Recurse -Force -Confirm:$false
